# Ejecutar en la terminal usando `python main.py`

# Solicitar al usuario que ingrese del número de elementos(notas)
numeroNotas = int(input("Ingrese el numero de notas: "))

# Lista de calificaciones
calificaciones = []

# Pedir al usuario que ingrese las notas una por una
for i in range(numeroNotas):
    calificacion = int(input(f"Ingrese la calificacion ({i + 1}/{numeroNotas}): "))
    calificaciones.append(calificacion)

# Calcular el promedio de las notas ingresadas (Se suma todo y se divide para el numero de elementos)
promedio = sum(calificaciones) / len(calificaciones)

# Mostrar el resultado del promedio al usuario
print(f"El promedio de calificaciones es: {promedio}")